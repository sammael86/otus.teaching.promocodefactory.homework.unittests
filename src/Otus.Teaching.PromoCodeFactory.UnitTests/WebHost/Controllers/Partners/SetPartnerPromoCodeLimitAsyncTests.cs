﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly Partner _partner;
        private readonly SetPartnerPromoCodeLimitRequest _request;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture()
                .Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture
                .Freeze<Mock<IRepository<Partner>>>();

            var partnerLimit = new Fixture()
                .Build<PartnerPromoCodeLimit>()
                .With(l=>l.CancelDate, (DateTime?)null)
                .Without(l => l.Partner)
                .Create();

            _partner = new Fixture()
                .Build<Partner>()
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>() { partnerLimit })
                .Create();
            
            _partnersRepositoryMock
                .Setup(x => x.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync((Partner)null);
            _partnersRepositoryMock
                .Setup(x => x.GetByIdAsync(_partner.Id))
                .ReturnsAsync(_partner);

            _partnersController = fixture
                .Build<PartnersController>()
                .OmitAutoProperties()
                .Create();

            _request = new Fixture()
                .Build<SetPartnerPromoCodeLimitRequest>()
                .Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.NewGuid();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            _partner.IsActive = false;

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partner.Id, _request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ActiveLimitIsNotNull_NumberIssuedPromoCodesEqualsZero()
        {
            // Arrange

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(_partner.Id, _request);

            // Assert
            _partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ActiveLimitIsNotNull_ActiveLimitCancelDateEqualsNow()
        {
            // Arrange

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(_partner.Id, _request);
            var cancelDate = _partner.PartnerLimits.FirstOrDefault(x => x.CancelDate.HasValue)?.CancelDate;

            // Assert
            cancelDate.Should().HaveValue().And.BeBefore(DateTime.Now);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-5)]
        public async void SetPartnerPromoCodeLimitAsync_NewLimitIsZeroOrSmaller_ReturnsBadRequest(int newLimit)
        {
            // Arrange
            _request.Limit = newLimit;

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partner.Id, _request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimitAdded_NewLimitSavedToDb()
        {
            // Arrange
            var initialLimitsCount = _partner.PartnerLimits.Count;

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(_partner.Id, _request);
            var newLimitsCount = _partner.PartnerLimits.Count;

            // Assert
            newLimitsCount.Should().Be(initialLimitsCount + 1);
            _partnersRepositoryMock.Verify(x => x.UpdateAsync(_partner), Times.Once);
        }
    }
}